#include <stdbool.h>
#include <stddef.h>
#define _DEFAULT_SOURCE

#include <stdio.h>
#include <stdlib.h>

#include "mem.h"
#include "mem_internals.h"

static size_t region_min_capacity() {
    return capacity_from_size((block_size){REGION_MIN_SIZE}).bytes;
}

static struct block_header *get_header(void *content) {
    return (struct block_header *)(content - offsetof(struct block_header, contents));
}

static void *get_content(struct block_header *block) {
    return (struct block_header *)((void *)block + offsetof(struct block_header, contents));
}

// Обычное успешное выделение памяти.
#define TEST_1_CAPACITY 100
static int test_malloc(void) {
    heap_init(REGION_MIN_SIZE);

    struct block_header *block = get_header(_malloc(TEST_1_CAPACITY));

    if (block == NULL || block->is_free || block->capacity.bytes < TEST_1_CAPACITY) {
        munmap(HEAP_START, REGION_MIN_SIZE);
        return 1;
    }
    _free(get_content(block));

    munmap(HEAP_START, size_from_capacity(block->capacity).bytes);

    return 0;
};

// Освобождение одного блока из нескольких выделенных.
#define TEST_2_CAPACITY_1 100
#define TEST_2_CAPACITY_2 200
#define TEST_2_CAPACITY_3 100
static int test_free_one_block(void) {
    heap_init(REGION_MIN_SIZE);

    struct block_header *block_1 = get_header(_malloc(TEST_2_CAPACITY_1));
    if (block_1 == NULL || block_1->is_free || block_1->capacity.bytes < TEST_2_CAPACITY_1) {
        munmap(HEAP_START, REGION_MIN_SIZE);
        return 1;
    }

    struct block_header *block_2 = get_header(_malloc(TEST_2_CAPACITY_2));
    if (block_2 == NULL || block_2->is_free || block_2->capacity.bytes < TEST_2_CAPACITY_2) {
        munmap(HEAP_START, REGION_MIN_SIZE);
        return 1;
    }

    struct block_header *block_3 = get_header(_malloc(TEST_2_CAPACITY_3));
    if (block_3 == NULL || block_3->is_free || block_3->capacity.bytes < TEST_2_CAPACITY_3) {
        munmap(HEAP_START, REGION_MIN_SIZE);
        return 1;
    }

    _free(get_content(block_3));

    if (!block_3->is_free) {
        munmap(HEAP_START, size_from_capacity(block_1->capacity).bytes +
                               size_from_capacity(block_2->capacity).bytes);
        return 1;
    }

    _free(get_content(block_2));

    if (!block_2->is_free || block_2->capacity.bytes < TEST_2_CAPACITY_2 + TEST_2_CAPACITY_3) {
        munmap(HEAP_START, size_from_capacity(block_1->capacity).bytes);
        return 1;
    }

    _free(get_content(block_1));

    munmap(HEAP_START, size_from_capacity(block_1->capacity).bytes);

    return 0;
};

// Освобождение двух блоков из нескольких выделенных.
#define TEST_3_CAPACITY_1 100
#define TEST_3_CAPACITY_2 200
#define TEST_3_CAPACITY_3 100
static int test_free_two_blocks(void) {
    heap_init(REGION_MIN_SIZE);

    struct block_header *block_1 = get_header(_malloc(TEST_2_CAPACITY_1));
    if (block_1 == NULL || block_1->is_free || block_1->capacity.bytes < TEST_2_CAPACITY_1) {
        munmap(HEAP_START, REGION_MIN_SIZE);
        return 1;
    }

    struct block_header *block_2 = get_header(_malloc(TEST_2_CAPACITY_2));
    if (block_2 == NULL || block_2->is_free || block_2->capacity.bytes < TEST_2_CAPACITY_2) {
        munmap(HEAP_START, REGION_MIN_SIZE);
        return 1;
    }

    struct block_header *block_3 = get_header(_malloc(TEST_2_CAPACITY_3));
    if (block_3 == NULL || block_3->is_free || block_3->capacity.bytes < TEST_2_CAPACITY_3) {
        munmap(HEAP_START, REGION_MIN_SIZE);
        return 1;
    }

    _free(get_content(block_3));

    if (!block_3->is_free) {
        munmap(HEAP_START, size_from_capacity(block_1->capacity).bytes +
                               size_from_capacity(block_2->capacity).bytes);
        return 1;
    }

    _free(get_content(block_2));

    if (!block_2->is_free || block_2->capacity.bytes < TEST_3_CAPACITY_2 + TEST_3_CAPACITY_3) {
        munmap(HEAP_START, size_from_capacity(block_1->capacity).bytes);
        return 1;
    }

    _free(get_content(block_1));

    munmap(HEAP_START, size_from_capacity(block_1->capacity).bytes);

    return 0;
};

// Память закончилась, новый регион памяти расширяет старый.
static int test_heap_grow_extends(void) {
    heap_init(REGION_MIN_SIZE);

    struct block_header *block_1 = get_header(_malloc(region_min_capacity()));
    _free(get_content(block_1));

    size_t reg_size = block_1->capacity.bytes;

    debug_heap(stdout, block_1);

    _malloc(reg_size);
    struct block_header *block_2 = get_header(_malloc(region_min_capacity()));
    debug_heap(stdout, block_1);

    if (block_1->next != block_2 ||
        block_1->contents + block_1->capacity.bytes != (void *)block_2) {
        munmap(block_1, size_from_capacity(block_1->capacity).bytes);
        munmap(block_2, size_from_capacity(block_2->capacity).bytes);
        return 1;
    }

    _free(get_content(block_2));

    munmap(block_1, size_from_capacity(block_1->capacity).bytes);
    munmap(block_2, size_from_capacity(block_2->capacity).bytes);

    return 0;
};

// Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона
// адресов, новый регион выделяется в другом месте.
static int test_heap_grow_no_extend(void) {
    heap_init(REGION_MIN_SIZE);

    struct block_header *block_1 = get_header(_malloc(region_min_capacity()));
    _free(get_content(block_1));

    size_t reg_size = block_1->capacity.bytes;

    void *addr =
        mmap(block_1->contents + block_1->capacity.bytes, REGION_MIN_SIZE, PROT_READ | PROT_WRITE,
             MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE, -1, 0);

    debug_heap(stdout, block_1);

    _malloc(reg_size);
    struct block_header *block_2 = get_header(_malloc(region_min_capacity()));
    debug_heap(stdout, block_1);

    if (block_1->next != block_2 ||
        block_1->contents + block_1->capacity.bytes == (void *)block_2) {

        munmap(block_1, size_from_capacity(block_1->capacity).bytes);
        munmap(addr, REGION_MIN_SIZE);
        munmap(block_2, size_from_capacity(block_2->capacity).bytes);

        return 1;
    }

    _free(get_content(block_2));

    munmap(block_1, size_from_capacity(block_1->capacity).bytes);
    munmap(addr, REGION_MIN_SIZE);
    munmap(block_2, size_from_capacity(block_2->capacity).bytes);

    return 0;
};

static struct {
    int (*run)(void);
    char *name;
} tests[] = {
    {test_malloc,              "Обычное успешное выделение памяти"                                          },
    {test_free_one_block,      "Освобождение одного блока из нескольких выделенных"          },
    {test_free_two_blocks,     "Освобождение двух блоков из нескольких выделенных"            },
    {test_heap_grow_extends,   "Память закончилась, новый регион памяти расширяет старый"},
    {test_heap_grow_no_extend,
     "Память закончилась, старый регион памяти не расширить из-за другого "
     "выделенного диапазона адресов, новый регион выделяется в другом месте." }
};

int run_tests(void) {
    int res = 0;

    for (size_t i = 0; tests[i].run && tests[i].name; i++) {
        printf("Runnig test\t%s\n", tests[i].name);
        int test_err = tests[i].run();
        res = res | test_err;
        printf("Result:\t\t%s\n", test_err ? "FAIL" : "OK");
        if (test_err) { fprintf(stderr, "Error:\t\t%d\n", test_err); }
        printf("\n");
    }

    return res != 0;
}
